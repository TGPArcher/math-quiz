﻿#pragma strict
import UnityEngine.UI;
import System.Collections.Generic;

var answerNumber : int;
var usedQuests = new List.<int>();
var quests = new List.<Hashtable>();
var obiectul : GameObject;
var componentul : Chose;

var image : GameObject;
var imageRellation : boolean = false;

var buttonA : GameObject;
var buttonB : GameObject;
var buttonC : GameObject;
var buttonD : GameObject;
var buttonQ : GameObject;
var continua : GameObject;
var corect : GameObject;
var menu : GameObject;
var incepe : GameObject;
var gresit : GameObject;
var scor : GameObject;

var verificaCorectitudinea : boolean;
var time = 0.0;
var conditie = false;

function Start () {
componentul = obiectul.GetComponent(Chose);
puncteRecord = PlayerPrefs.GetInt("PuncteRecord");
}

function Update () {
	if (puncteRecord - 3 < 0) {
		rasObj.GetComponent(Button).interactable = false;
		if (puncteRecord - 2 < 0) {
			rasObj.GetComponent(Button).interactable = false;
			twoVarObj.GetComponent(Button).interactable = false;
			if(puncteRecord - 1 < 0) {
				rasObj.GetComponent(Button).interactable = false;
				twoVarObj.GetComponent(Button).interactable = false;
				oneVarObj.GetComponent(Button).interactable = false;
			}
		}
	}	
PlayerPrefs.SetInt("PuncteRecord", puncteRecord);

if ( imageRellation == true) {
	image.gameObject.SetActive(true);
	}
answerNumber = componentul.answerShuffle;
usedQuests = componentul.usedQuests;
quests = componentul.quests;

	if (conditie == true) {
		time = time + Time.deltaTime;
			if (time >= 2.5) {
				time = 0.0;
				conditie = false;
				buttonA.gameObject.SetActive(false);
				buttonB.gameObject.SetActive(false);
				buttonC.gameObject.SetActive(false);
				buttonD.gameObject.SetActive(false);
				buttonQ.gameObject.SetActive(false);
				buttonA.GetComponent.<Image>().color = Color.white;
				buttonB.GetComponent.<Image>().color = Color.white;
				buttonC.GetComponent.<Image>().color = Color.white;
				buttonD.GetComponent.<Image>().color = Color.white;
				buttonA.GetComponent(Button).interactable = true;
				buttonB.GetComponent(Button).interactable = true;
				buttonC.GetComponent(Button).interactable = true;
				buttonD.GetComponent(Button).interactable = true;
				uneltele.GetComponent(Button).interactable = false;
				twoVarObj.gameObject.SetActive(false);
				oneVarObj.gameObject.SetActive(false);
				rasObj.gameObject.SetActive(false);
				panel.gameObject.SetActive(false);
				inchideText.gameObject.SetActive(false);
				unelteText.gameObject.SetActive(true);
				if(verificaCorectitudinea == true) {
					corect.gameObject.SetActive(true);
					gresit.gameObject.SetActive(false);
					continua.gameObject.SetActive(true);
					menu.gameObject.SetActive(false);
					incepe.gameObject.SetActive(false);
					scor.gameObject.SetActive(true);
					}
				else {
					corect.gameObject.SetActive(false);
					gresit.gameObject.SetActive(true);
					continua.gameObject.SetActive(false);
					menu.gameObject.SetActive(true);
					incepe.gameObject.SetActive(false);
					scor.gameObject.SetActive(true);
					}
			}
	}
	PlayerPrefs.Save();
}
													// functiile uneltelor
var twoVarObj : GameObject;
var oneVarObj : GameObject;
var rasObj : GameObject;
var inchideText : GameObject;
var unelteText : GameObject;
var panel : GameObject;
var uneltele : GameObject;
// functia care pune in miscare butonul de unelte
function unelte () {
	if(inchideText.activeInHierarchy) {
			twoVarObj.gameObject.SetActive(false);
			oneVarObj.gameObject.SetActive(false);
			rasObj.gameObject.SetActive(false);
			inchideText.gameObject.SetActive(false);
			panel.gameObject.SetActive(false);
			unelteText.gameObject.SetActive(true);
	}
	else {
			twoVarObj.gameObject.SetActive(true);
			oneVarObj.gameObject.SetActive(true);
			rasObj.gameObject.SetActive(true);
			inchideText.gameObject.SetActive(true);
			panel.gameObject.SetActive(true);
			unelteText.gameObject.SetActive(false);
	}
}
// functia care reduce 2 variante
function twoVar () {
	puncteRecord -= 2;
	twoVarObj.GetComponent(Button).interactable = false;
	oneVarObj.GetComponent(Button).interactable = false;
	var variants = Random.Range(0, 3);
	switch (answerNumber) {
		case 1: 
			if (variants == 0) {
				buttonB.gameObject.SetActive(false);
				buttonC.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonC.gameObject.SetActive(false);
				buttonD.gameObject.SetActive(false);
				}
			else {
				buttonD.gameObject.SetActive(false);
				buttonB.gameObject.SetActive(false);
				}
			break;
		case 2:
			if (variants == 0) {
				buttonB.gameObject.SetActive(false);
				buttonC.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonC.gameObject.SetActive(false);
				buttonA.gameObject.SetActive(false);
				}
			else {
				buttonA.gameObject.SetActive(false);
				buttonB.gameObject.SetActive(false);
				}
			break;
		case 3:
			if (variants == 0) {
				buttonD.gameObject.SetActive(false);
				buttonC.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonC.gameObject.SetActive(false);
				buttonA.gameObject.SetActive(false);
				}
			else {
				buttonA.gameObject.SetActive(false);
				buttonD.gameObject.SetActive(false);
				}
			break;
		case 4:
			if (variants == 0) {
				buttonB.gameObject.SetActive(false);
				buttonD.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonD.gameObject.SetActive(false);
				buttonA.gameObject.SetActive(false);
				}
			else {
				buttonA.gameObject.SetActive(false);
				buttonB.gameObject.SetActive(false);
				}
			break;
	}
}
// functia care reduce o varianta
function oneVar () {
	puncteRecord -= 1;
	oneVarObj.GetComponent(Button).interactable = false;
	var variants = Random.Range(0, 3);
	switch (answerNumber) {
		case 1: 
			if (variants == 0) {
				buttonB.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonC.gameObject.SetActive(false);
				}
			else {
				buttonD.gameObject.SetActive(false);
				}
			break;
		case 2:
			if (variants == 0) {
				buttonB.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonC.gameObject.SetActive(false);
				}
			else {
				buttonA.gameObject.SetActive(false);
				}
			break;
		case 3:
			if (variants == 0) {
				buttonD.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonC.gameObject.SetActive(false);
				}
			else {
				buttonA.gameObject.SetActive(false);
				}
			break;
		case 4:
			if (variants == 0) {
				buttonB.gameObject.SetActive(false);
				}
			else if (variants == 1) {
				buttonD.gameObject.SetActive(false);
				}
			else {
				buttonA.gameObject.SetActive(false);
				}
			break;
	}
}
// functia care da raspunsul
function raspunsul () {
	puncteRecord -= 3;
	rasObj.GetComponent(Button).interactable = false;
	twoVarObj.GetComponent(Button).interactable = false;
	oneVarObj.GetComponent(Button).interactable = false;
	switch (answerNumber) {
		case 1:
			buttonB.gameObject.SetActive(false);
			buttonC.gameObject.SetActive(false);
			buttonD.gameObject.SetActive(false);
		break;
		case 2:
			buttonA.gameObject.SetActive(false);
			buttonC.gameObject.SetActive(false);
			buttonB.gameObject.SetActive(false);
		break;
		case 3:
			buttonA.gameObject.SetActive(false);
			buttonC.gameObject.SetActive(false);
			buttonD.gameObject.SetActive(false);
		break;
		case 4:
			buttonA.gameObject.SetActive(false);
			buttonD.gameObject.SetActive(false);
			buttonB.gameObject.SetActive(false);
		break;
	}
}
// functiile de verificare a butoanelor.
// variabila care reprezinta scorul pe runda.
var scorActual : int = 0;
// variabile care contin scorul actual si scorul record
var puncteActual : int = 0;
var puncteRecord : int = 0;
function RaspunsulA () {
	switch (answerNumber) {
		case 1:
			buttonA.GetComponent.<Image>().color = Color.green;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = true;
			scorActual += 1;
			puncteRecord += 1;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 2:
			buttonA.GetComponent.<Image>().color = Color.blue;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.green;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 3:
			buttonA.GetComponent.<Image>().color = Color.blue;
			buttonB.GetComponent.<Image>().color = Color.green;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 4:
			buttonA.GetComponent.<Image>().color = Color.blue;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.green;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
		}
	conditie = true;
}
function RaspunsulB () {
	switch (answerNumber) {
		case 1:
			buttonA.GetComponent.<Image>().color = Color.green;
			buttonB.GetComponent.<Image>().color = Color.blue;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 2:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.blue;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.green;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 3:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.green;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = true;
			scorActual += 1;
			puncteRecord += 1;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 4:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.blue;
			buttonC.GetComponent.<Image>().color = Color.green;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
		}
	conditie = true;
}
function RaspunsulC () {
	switch (answerNumber) {
		case 1:
			buttonA.GetComponent.<Image>().color = Color.green;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.blue;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 2:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.blue;
			buttonD.GetComponent.<Image>().color = Color.green;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 3:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.green;
			buttonC.GetComponent.<Image>().color = Color.blue;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 4:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.green;
			buttonD.GetComponent.<Image>().color = Color.red;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = true;
			scorActual += 1;
			puncteRecord += 1;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
		}
	conditie = true;
}
function RaspunsulD () {
	switch (answerNumber) {
		case 1:
			buttonA.GetComponent.<Image>().color = Color.green;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.blue;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 2:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.green;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = true;
			scorActual += 1;
			puncteRecord += 1;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 3:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.green;
			buttonC.GetComponent.<Image>().color = Color.red;
			buttonD.GetComponent.<Image>().color = Color.blue;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
			break;
		case 4:
			buttonA.GetComponent.<Image>().color = Color.red;
			buttonB.GetComponent.<Image>().color = Color.red;
			buttonC.GetComponent.<Image>().color = Color.green;
			buttonD.GetComponent.<Image>().color = Color.blue;
			buttonA.GetComponent(Button).interactable = false;
			buttonB.GetComponent(Button).interactable = false;
			buttonC.GetComponent(Button).interactable = false;
			buttonD.GetComponent(Button).interactable = false;
			verificaCorectitudinea = false;
			if (usedQuests.Count == quests.Count) {
				imageRellation = true;
			}
		}
	conditie = true;
}