﻿#pragma strict
import UnityEngine.UI;

// Obiectul de unde voi lua scorul
var obiectul : GameObject;
var Componentul : AnswerScript;
// Variabila de scor
var scorActual : int;
var scorulRecord : int;
// Variabila de puncte
var puncteRecord : int;
// variabile text unde va fi afisat scorul
var obiectulScorActual : GameObject;
var obiectulScorRecord : GameObject;
var componentulScorActual : Text;
var componentulScorRecord : Text;
// variabile text unde vor fi afisate punctele
var obiectulPuncte : GameObject;
var componentulPuncte : Text;
// Obiectul text record
var textRecord : GameObject;

function Start () {
// Atribuirea componentelor text si script
Componentul = obiectul.GetComponent(AnswerScript);
componentulScorActual = obiectulScorActual.GetComponent(Text);
componentulScorRecord = obiectulScorRecord.GetComponent(Text);
scorulRecord = PlayerPrefs.GetInt("ScorulRecord");
// Atribuirea componentelor text si script
componentulPuncte = obiectulPuncte.GetComponent(Text);
}

function Update () {
// atribuirea scorului
scorActual = Componentul.scorActual;
// atribuirea punctelor
puncteRecord = Componentul.puncteRecord;
// scorul record
	if(scorActual > scorulRecord) {
		PlayerPrefs.SetInt("ScorulRecord", scorActual);
		componentulScorActual.text = scorActual.ToString();
		componentulScorRecord.text = PlayerPrefs.GetInt("ScorulRecord").ToString();
		componentulScorRecord.color = Color.red;
		textRecord.GetComponent(Text).color = Color.red;
		}
	else {
		componentulScorActual.text = scorActual.ToString();
		componentulScorRecord.text = PlayerPrefs.GetInt("ScorulRecord").ToString();
		}
		PlayerPrefs.Save();
componentulPuncte.text = puncteRecord.ToString();
}