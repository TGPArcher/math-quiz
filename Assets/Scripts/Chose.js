﻿#pragma strict
import System.Collections.Generic;
import UnityEngine.UI;

// Se adauga o lista de obiecte care va fi ca suport pentru cea care va verifica corectitudinea.
var newHastable = new List.<Hashtable>();
// Obiectul care are scriptul cu intrebari
var Obiectul : GameObject;
private var Componentul : Questions;
// Variabilele care raspund de selectarea intrebari si ordini raspunsurilor.
public var questionSelector : int;
public var answerShuffle : int;
// Lista de questuri trecuta in aceste variabile
public var quests = new List.<Hashtable>();
public var selectedQuest = new Hashtable();
// Butoanele UI ca obiecte:
var buttonTextA : GameObject;
var buttonTextB : GameObject;
var buttonTextC : GameObject;
var buttonTextD : GameObject;
var buttonQ : GameObject;
// Scriptul text al butoanelor...
var aText : Text;
var bText : Text;
var cText : Text;
var dText : Text;
var qText : Text;
// Lista de questuri adaugate pe rind care au fost folosite
var usedQuests = new List.<int>();
usedQuests.Add(0);

function Start () {
// Se atribuie lista de questuri din scriptul Questions
Componentul = Obiectul.GetComponent(Questions);
quests = Componentul.allQuests;
// Se atribuie componetul text al butoanelor la variabila...
aText = buttonTextA.GetComponent.<Text>();
bText = buttonTextB.GetComponent.<Text>();
cText = buttonTextC.GetComponent.<Text>();
dText = buttonTextD.GetComponent.<Text>();
qText = buttonQ.GetComponent.<Text>();
}

function Update () {
// Se alege una din lista questuri

	selectedQuest = quests[questionSelector];
	
	switch (answerShuffle) {
		case 1: 
			aText.text = selectedQuest[2].ToString();
			bText.text = selectedQuest[3].ToString();
			cText.text = selectedQuest[4].ToString();
			dText.text = selectedQuest[5].ToString();
			qText.text = selectedQuest[1].ToString();
			break;
		case 2:
			aText.text = selectedQuest[5].ToString();
			bText.text = selectedQuest[4].ToString();
			cText.text = selectedQuest[3].ToString();
			dText.text = selectedQuest[2].ToString();
			qText.text = selectedQuest[1].ToString();
			break;
		case 3:
			aText.text = selectedQuest[4].ToString();
			bText.text = selectedQuest[2].ToString();
			cText.text = selectedQuest[5].ToString();
			dText.text = selectedQuest[3].ToString();
			qText.text = selectedQuest[1].ToString();
			break;
		case 4:
			aText.text = selectedQuest[3].ToString();
			bText.text = selectedQuest[5].ToString();
			cText.text = selectedQuest[2].ToString();
			dText.text = selectedQuest[4].ToString();
			qText.text = selectedQuest[1].ToString();
			break;
		}
}
///butoanele de la unelte
var twoVarObj : GameObject;
var oneVarObj : GameObject;
var rasObj : GameObject;
var unelte : GameObject;
////////////////////////////////
var continuaButton : GameObject;
var incepeButton : GameObject;
var corectText : GameObject;
var gresitText : GameObject;
var scor : GameObject;
var buttonA : GameObject;
var buttonB : GameObject;
var buttonC : GameObject;
var buttonD : GameObject;
function Next () {
// se activeaza butoanele pt intrebare
	twoVarObj.GetComponent(Button).interactable = true;
	oneVarObj.GetComponent(Button).interactable = true;
	rasObj.GetComponent(Button).interactable = true;
	unelte.GetComponent(Button).interactable = true;
// Se alege un numar la intimplare care va reprezenta questul.
	questionSelector = Random.Range(1, quests.Count);
// partea in care se va verifica daca a mai fost aceasta intrebare.
	while (usedQuests.Contains(questionSelector)) {
		questionSelector = Random.Range(1, quests.Count);
		}
	if (!usedQuests.Contains(questionSelector)) {
		usedQuests.Add(questionSelector);
		}
// Se alege un numar la intimplare care va reprezenta ordinea raspunsurilor.
	answerShuffle = Random.Range(1, 5);
// Obiectele care vor fi inchise sau deschise.
	continuaButton.gameObject.SetActive(false);
	incepeButton.gameObject.SetActive(false);
	corectText.gameObject.SetActive(false);
	gresitText.gameObject.SetActive(false);
	scor.gameObject.SetActive(false);
	buttonA.gameObject.SetActive(true);
	buttonB.gameObject.SetActive(true);
	buttonC.gameObject.SetActive(true);
	buttonD.gameObject.SetActive(true);
	buttonQ.gameObject.SetActive(true);
	}