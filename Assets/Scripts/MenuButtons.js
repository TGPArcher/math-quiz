﻿#pragma strict

var start : GameObject;
var quit : GameObject;
var scor : GameObject;
var panel : GameObject;

var textScor : GameObject;
var textComponent : Text;

function Start () {
textComponent = textScor.GetComponent(Text);
}

function Update () {
	textComponent.text = PlayerPrefs.GetInt("ScorulRecord").ToString();
}

function StartGame () {
	Application.LoadLevel ("Game") ;
	}
function MenuGame () {
	Application.LoadLevel ("Start Menu") ;
	}
function QuitGame () {
	Application.Quit();
	}
function DeleteStats () {
	PlayerPrefs.DeleteKey("ScorulRecord");
}
function ScorRecord () {
	start.gameObject.SetActive(false);
	quit.gameObject.SetActive(false);
	scor.gameObject.SetActive(false);
	panel.gameObject.SetActive(true);
	}
function Back () {
	start.gameObject.SetActive(true);
	quit.gameObject.SetActive(true);
	scor.gameObject.SetActive(true);
	panel.gameObject.SetActive(false);
	}